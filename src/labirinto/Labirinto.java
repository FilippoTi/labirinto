package labirinto;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Labirinto
{

    //Variabili globali
    static int Labirinto[][];
    
    static int XStart;
    static int YStart;
    
    static int XEnd;
    static int YEnd;
    
    static int NumeroRighe;
    static int NumeroColonne;
    
    public static void CaricaElementi(String NomeFile) throws FileNotFoundException, IOException    //Mi leggo il file e lo carico
    {
        int Riga = 0;
        int Colonna = 0;
        
        FileReader File = new FileReader(NomeFile);
        Scanner FileIn = new Scanner(File);
        
        NumeroColonne = FileIn.nextInt();
        NumeroRighe = FileIn.nextInt();
        
        Labirinto = new int[NumeroRighe][NumeroColonne];
        
        for (Riga = 0; Riga < NumeroRighe; Riga++)
        {
            for (Colonna = 0; Colonna < NumeroColonne; Colonna++)
            {
                Labirinto[Riga][Colonna] = FileIn.nextInt();
            }
        }
        
        YStart = FileIn.nextInt();
        XStart = FileIn.nextInt();
        
        YEnd = FileIn.nextInt();
        XEnd = FileIn.nextInt();
        
        File.close();
        
        return;
    }
    
    public static void Stampa(int NRighe, int NColonne) //Stampa a video
    {
        int Riga = 0;
        int Colonna = 0;
        
        for (Riga = 0; Riga < NRighe; Riga++)
        {
            for (Colonna = 0; Colonna < NColonne; Colonna++)
            {
                if (Labirinto[Riga][Colonna] == 1)
                {
                    System.out.print((char) 35 + " ");
                } else if (Labirinto[Riga][Colonna] == 0)
                {
                    System.out.print("  ");
                } else
                {
                    System.out.print(Labirinto[Riga][Colonna] + " ");
                }
                
            }
            System.out.println("");
        }
        
        System.out.println(XStart + " " + YStart + "\n" + XEnd + " " + YEnd);
        
        return;
    }
    
    public static void Coord() throws IOException
    {
        File f = new File("Soluzione.dat");
        FileWriter fw = new FileWriter(f);
        BufferedWriter bw = new BufferedWriter(fw);
        int Riga, Colonna;
        
        int NumeroSoluzione = 0;
        
        for (Riga = 0; Riga < NumeroRighe; Riga++)
        {
            for (Colonna = 0; Colonna < NumeroColonne; Colonna++)
            {
                if (Labirinto[Riga][Colonna] == 0)
                {
                    Labirinto[Riga][Colonna] = 1;
                } else if (Labirinto[Riga][Colonna] == 3)
                {
                    Labirinto[Riga][Colonna] = 2;
                    NumeroSoluzione++;
                } else if (Labirinto[Riga][Colonna] == 2)
                {
                    NumeroSoluzione++;
                }
            }
        }        
        
        bw.write(NumeroSoluzione);
        
        Riga = XStart;
        Colonna = YStart;
        while (1==1)
        {
            if (Riga != 0 && Labirinto[Riga - 1][Colonna] == 2)
            {
                bw.write(Riga);
                bw.write(" ");
                bw.write(Colonna);
                bw.write("\n");
                Labirinto[Riga][Colonna] = 4;
                Riga--;
            } else if (Labirinto[Riga + 1][Colonna] == 2)
            {
                bw.write(Riga + " " + Colonna + "\n");
                Labirinto[Riga][Colonna] = 4;
                Riga++;
            } else if (Colonna != 0 && Labirinto[Riga][Colonna - 1] == 2)
            {
                bw.write(Riga + " " + Colonna + "\n");
                Labirinto[Riga][Colonna] = 4;
                Colonna--;
            } else if (Labirinto[Riga][Colonna + 1] == 2)
            {
                bw.write(Riga + " " + Colonna + "\n");
                Labirinto[Riga][Colonna] = 4;
                Colonna++;
            }
            
            if(Colonna == YEnd && Riga == XEnd)
            {
                break;
            }
        }
        bw.flush();
        bw.close();
    }
    
    public static void Risolvi()
    {
        int IndiceRiga = XStart;
        int IndiceColonna = YStart;
        
        int ProxColonna = -1;
        int ProxRiga = -1;
        
        int numeroStrade = 0;
        
        int i;
        
        while (1 == 1)
        {
            //Stampa(NumeroRighe, NumeroColonne);
            if (Labirinto[IndiceRiga][IndiceColonna] == 0 || Labirinto[IndiceRiga][IndiceColonna] == 2 || Labirinto[IndiceRiga][IndiceColonna] == 3)
            {
                if (IndiceRiga != 0 && Labirinto[IndiceRiga - 1][IndiceColonna] == 0)
                {
                    numeroStrade++;
                    if (ProxRiga == -1 && ProxColonna == -1)
                    {
                        ProxColonna = IndiceColonna;
                        ProxRiga = IndiceRiga - 1;
                    }
                }
                
                if (Labirinto[IndiceRiga + 1][IndiceColonna] == 0)
                {
                    numeroStrade++;
                    if (ProxRiga == -1 && ProxColonna == -1)
                    {
                        ProxColonna = IndiceColonna;
                        ProxRiga = IndiceRiga + 1;
                    }
                }
                
                if (IndiceColonna != 0 && Labirinto[IndiceRiga][IndiceColonna - 1] == 0)
                {
                    numeroStrade++;
                    if (ProxRiga == -1 && ProxColonna == -1)
                    {
                        ProxColonna = IndiceColonna - 1;
                        ProxRiga = IndiceRiga;
                    }
                }
                
                if (Labirinto[IndiceRiga][IndiceColonna + 1] == 0)
                {
                    numeroStrade++;
                    if (ProxRiga == -1 && ProxColonna == -1)
                    {
                        ProxColonna = IndiceColonna + 1;
                        ProxRiga = IndiceRiga;
                    }
                }
                
                if (numeroStrade == 1)
                {
                    Labirinto[IndiceRiga][IndiceColonna] = 2;
                    
                    IndiceRiga = ProxRiga;
                    IndiceColonna = ProxColonna;
                    
                } else if (numeroStrade >= 1)
                {
                    Labirinto[IndiceRiga][IndiceColonna] = 3;
                    
                    IndiceRiga = ProxRiga;
                    IndiceColonna = ProxColonna;
                } else if (numeroStrade == 0)
                {
                    while (Labirinto[IndiceRiga][IndiceColonna] != 3)
                    {
                        //Stampa(NumeroRighe, NumeroColonne);
                        if (Labirinto[IndiceRiga + 1][IndiceColonna] == 2 || Labirinto[IndiceRiga + 1][IndiceColonna] == 3)
                        {
                            Labirinto[IndiceRiga][IndiceColonna] = 1;
                            IndiceRiga = IndiceRiga + 1;
                        } else if (IndiceRiga != 0 && Labirinto[IndiceRiga - 1][IndiceColonna] == 2 || Labirinto[IndiceRiga - 1][IndiceColonna] == 3)
                        {
                            Labirinto[IndiceRiga][IndiceColonna] = 1;
                            IndiceRiga = IndiceRiga - 1;
                        } else if (IndiceColonna != 0 && Labirinto[IndiceRiga][IndiceColonna - 1] == 2 || Labirinto[IndiceRiga][IndiceColonna - 1] == 3)
                        {
                            Labirinto[IndiceRiga][IndiceColonna] = 1;
                            IndiceColonna = IndiceColonna - 1;
                            
                        } else if (Labirinto[IndiceRiga][IndiceColonna + 1] == 2 || Labirinto[IndiceRiga][IndiceColonna + 1] == 3)
                        {
                            Labirinto[IndiceRiga][IndiceColonna] = 1;
                            IndiceColonna = IndiceColonna + 1;
                        }
                    }
                }
            }
            
            ProxRiga = -1;
            ProxColonna = -1;
            numeroStrade = 0;
            
            if (IndiceRiga == XEnd && IndiceColonna == YEnd)
            {
                Labirinto[XEnd][YEnd] = 2;
                break;
            }
        }
    }
    
    public static void main(String[] args) throws IOException
    {
        String NomeFile = "maze1.dat";
        
        CaricaElementi(NomeFile);
        //Stampa(NumeroRighe, NumeroColonne);
        Risolvi();
        Coord();
        Stampa(NumeroRighe, NumeroColonne);
    }
}
